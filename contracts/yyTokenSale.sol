pragma solidity ^0.5.0;
import './yyToken.sol';
contract yyTokenSale {
    address admin;
    yyToken public yytokenContract;
    uint256 public tokenPrice;
    constructor(yyToken _yytokenContract, uint256 _tokenPrice) public {
        admin = msg.sender;
        yytokenContract = _yytokenContract;
        tokenPrice = _tokenPrice;
    }
}