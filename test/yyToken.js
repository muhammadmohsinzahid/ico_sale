var yyToken = artifacts.require('./yyToken');

contract('yyToken', (accounts) => {
    var tokenIns;
    it('initializes the contract with the correct values', async () => {
        tokenIns = await yyToken.deployed();
        assert.equal(await tokenIns.name(),'yyToken','It has correct name');
    });

    it('checking the symbol', async () => {
        assert.equal(await tokenIns.symbol(),'YY','It has correct symbol');
    });
    
    it('checking the standard', async () => {
        assert.equal(await tokenIns.standard(),'YY Version 1.0','It has correct standard');
    });

    it('checking the total supply on deployment', async () => {
        
       
        totalSupply = await tokenIns.totalSupply();
        assert.equal(totalSupply.toNumber(), 1000000, 'it is 1M');
       
        // return yyToken.deployed().then((i) => {
        //     //tokenIns = i;
        //     return i.totalSupply();
        // }).then((totalSupply) => {
        //     //console.log(totalSupply.toNumber());
        //     assert.equal(totalSupply.toNumber(), 1000000, 'it is 1M');
        // });
    });

    it('transfering tokens', async () => {
        try {
            transferAmount = 1000;
            _to = accounts[1];
            _from = accounts[0];
            transferResult = await tokenIns.transfer(_to,transferAmount);
            assert(tokenIns.balanceOf(_to),transferAmount,'Successfully added');
        } catch(error) {
            assert(error.message.indexOf('revert') > 0,'error message');
        }
    });

    it('approve token for delegated transfer', async () => {
        receipt = await tokenIns.approve(accounts[1],200);
        //console.log(JSON.stringify(receipt,null,4));
        assert.equal(receipt.logs.length, 1, 'trigger one event');
        assert.equal(receipt.logs[0].event, 'Approval', 'should be "Approval" event');
        assert.equal(receipt.logs[0].args._owner, accounts[0], 'Authorized owner');
        assert.equal(receipt.logs[0].args._spender, accounts[1], 'Authorized spender');
        assert.equal(receipt.logs[0].args._value, 200, 'spending amount is 200');
       
        allowance = await tokenIns.allowance(accounts[0], accounts[1]);
        assert.equal(allowance.toNumber(), 200, 'store allownance for delegate transfer');
    });

    it('handle the delegate transfer', async () => {
        fromAcc = accounts[2];
        toAcc = accounts[3];
        spendingAcc = accounts[4];
        receipt = await tokenIns.transfer(fromAcc, 2500 , {from:accounts[0]});
        
        allowance = await tokenIns.approve(spendingAcc, 20, {from: fromAcc});
        receipt = await tokenIns.transferFrom(fromAcc, toAcc , 20, {from:spendingAcc});
        balanceOfToAcc = await tokenIns.balanceOf(toAcc);
        assert.equal(balanceOfToAcc.toNumber(), 20, 'Successfully transfer amount');
        newAllowance = await tokenIns.allowance(fromAcc,spendingAcc);
        assert.equal(newAllowance.toNumber(),0,'Already transferred, so it must be ZERO');
    });

});