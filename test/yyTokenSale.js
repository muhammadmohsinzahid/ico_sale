var yyTokenSale = artifacts.require('./yyTokenSale');

contract('yyTokenSale', (accounts) => {
    var tokenSaleIns;
    
    it('initializes the contract with correct values', async () => {
        tokenSaleIns = await yyTokenSale.deployed();
        assert.notEqual(tokenSaleIns.address,0x0,'has valid address');
        yyTokenContract = tokenSaleIns.yytokenContract();
        assert.notEqual(yyTokenContract,0x0, 'has valid yyToken Address');
        price = await tokenSaleIns.tokenPrice();
        assert.equal(price, 1000000000000000, 'Price in wei');
    });

});