const yyToken = artifacts.require("yyToken");
const yyTokenSale = artifacts.require("yyTokenSale");

module.exports = async function(deployer) {
  deployer.deploy(yyToken, 1000000).then(() => {
    return deployer.deploy(yyTokenSale, yyToken.address, 1000000000000000);
  });
  
};